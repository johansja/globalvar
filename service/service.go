package service

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/johansja/globalvar/repo"
)

func Serve() {
	log.Debug().Msg("Service package.")

	log.Error().Str("global", repo.Global).Msgf("Repo var.")

	repo.Get()

	repo.Global = "set by service"

	log.Error().Str("global", repo.Global).Msgf("Repo var.")
}
