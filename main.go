package main

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/johansja/globalvar/repo"
	"gitlab.com/johansja/globalvar/service"
)

func main() {
	zerolog.LevelFieldName = "severity"
	log.Logger = log.With().Caller().Logger()

	repo.Global = "set by main"

	log.Info().Msg("Main function.")
	service.Serve()

	log.Error().Str("global", repo.Global).Msgf("Repo var.")
}
